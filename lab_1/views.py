from django.shortcuts import render
from .forms import PortfolioForm
from .models import Portfolio
import datetime

def portfolio_form(request):
    form = PortfolioForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        form = PortfolioForm()
    form_output = Portfolio.objects.all()
    date = datetime.datetime.now()
    print(date)
    context = {
        'portfolio_form' : form,
        'portfolio_form_output': form_output,
        'date': date
    }
    return render(request, "portfolio_form.html", context)

def profile(request):
    return render(request, 'profile.html')